import 'package:flutter/material.dart';
import '../../model/product.dart';


class ShoppingCartPage extends StatelessWidget {

  const ShoppingCartPage({super.key});

  @override
  Widget build(BuildContext context) {

final List<Product> items = [
    Product(id: '1', name: 'Item 1', price: 10000,  image: 'assets/images/shoes1.jpg'),
    Product(id: '2', name: 'Item 2', price: 20000,  image: 'assets/images/shoes2.jpg'),
    Product(id: '3', name: 'Item 3', price: 30000,  image: 'assets/images/shoes3.jpg'),
    Product(id: '4', name: 'Item 4', price: 40000,  image: 'assets/images/shoes1.jpg'),
    Product(id: '5', name: 'Item 5', price: 50000,  image: 'assets/images/shoes2.jpg'),
    Product(id: '6', name: 'Item 6', price: 60000,  image: 'assets/images/shoes3.jpg'),
    Product(id: '7', name: 'Item 7', price: 70000,  image: 'assets/images/shoes1.jpg'),
    Product(id: '8', name: 'Item 8', price: 80000,  image: 'assets/images/shoes2.jpg'),
    Product(id: '9', name: 'Item 9', price: 90000,  image: 'assets/images/shoes3.jpg'),
    Product(id: '10', name: 'Item 10', price: 100000,  image: 'assets/images/shoes1.jpg'),
    Product(id: '11', name: 'Item 11', price: 100000,  image: 'assets/images/shoes2.jpg'),
    Product(id: '12', name: 'Item 12', price: 100000,  image: 'assets/images/shoes3.jpg'),
  ];




    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {},
          tooltip: 'Back',
        ),
        title: const Text('Shopping Cart'),
      ),
      body: ListView.builder(itemCount: items.length ,itemBuilder: (BuildContext context, int index){
        return Card(
          child: Column(
            children: [
              ListTile(
                leading: Image.asset(items[index].image, height: 56.8, width: 56.8, fit: BoxFit.cover,),
                title: Text(items[index].name),
                subtitle: Text('Rp${items[index].price}',
                style: const TextStyle(fontWeight: FontWeight.bold),),
                trailing: IconButton(
                  icon: const Icon(Icons.delete),
                  onPressed: () {},
                ),
              ),
            ShoppingCartItemQty()
            ],
          ),
        );
      })
    );
  }
}


class ShoppingCartItemQty extends StatefulWidget {
  const ShoppingCartItemQty({super.key});

  @override
  State<ShoppingCartItemQty> createState() => _ShoppingCartItemQtyState();
}

class _ShoppingCartItemQtyState extends State<ShoppingCartItemQty> {
  int _qty = 1;
  
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        IconButton(icon: Icon(Icons.delete),onPressed: () {
          
        },),
        IconButton(
          icon: const Icon(Icons.remove),
          onPressed: () {
            setState(() {
              if (_qty > 1) {
                _qty--;
              }
            });
          },
        ),
        Text('$_qty'),
        IconButton(
          icon: const Icon(Icons.add),
          onPressed: () {
            setState(() {
              _qty++;
            });
          },
        ),
      ],
      
    );
  }
}